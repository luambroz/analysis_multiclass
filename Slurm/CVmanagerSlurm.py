import os

njets = 2
ptv_region = "_ptv_150_250"

max_depths = [3, 4, 5]
gammas = [0, 4]
min_child_weights = [1]
learning_rates = [0.005, 0.01, 0.1]

index = 0
for max_depth in max_depths:
  for gamma in gammas:
    for min_child_weight in min_child_weights:
      for learning_rate in learning_rates:
        fname = "jobCV" + str(index) + ".sh"

        f = open(fname,"w")
        f.write("#!/bin/bash \n")
        f.write("#SBATCH --nodes=1 \n")
        f.write("#SBATCH --ntasks-per-node=16 \n")
        f.write("#SBATCH --time=06:00:00 \n")
        f.write("source /home/univ4207/setup_env.sh \n")
        f.write("python MVA_CV.py -njets " + str(njets) + " -ptv " + ptv_region + " -max_depth " + str(max_depth) + " -min_child_weight " + str(min_child_weight) + " -gamma " + str(gamma) + " -learning_rate " + str(learning_rate) + " \n")
        f.close()
        
        os.system("sbatch " + fname)
        os.system("rm " + str(fname))
        index+=1

