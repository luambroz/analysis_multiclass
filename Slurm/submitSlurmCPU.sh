#!/bin/bash
## # #SBATCH --nodes=1
## # #SBATCH --ntasks-per-node=16
#SBATCH --time=2:00:00
#SBATCH --job-name=Multiclass

source /home/univ4207/setup_env.sh 

echo "Starting!"
START_TIME=$SECONDS

#python MVA.py -njets 2  -ptv _ptv_150_250
python MVA.py -njets 2  -ptv _ptv_250
#python MVA.py -njets 3  -ptv _ptv_150_250
#python MVA.py -njets 3  -ptv _ptv_250
#python MVA.py -njets 2  -ptv 
#python MVA.py -njets 2  -ptv

ELAPSED_TIME=$(($SECONDS - $START_TIME))
echo "Finished!" 
echo "$(($ELAPSED_TIME/60)) min $(($ELAPSED_TIME%60)) sec" 

#To send the job: sbatch submitSlurmCPU.sh 
