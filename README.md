# Multiclass Analysis

A series of scripts to train and evaluate a multiclassifier for the VHbb analysis.

## Prerequisites

The packages required to run can be install with conda:

```
conda create -n multiclass_env --file requirements.txt
```

