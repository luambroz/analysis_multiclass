#!/bin/bash

source /home/ambroz/setup_env.sh

echo "Starting!"
START_TIME=$SECONDS

cd /home/ambroz/VHbb/analysis_multiclass

#python MVA.py -njets 2  -ptv _ptv_150_250 -fold 1 -weights AbsWeights -mBB_max 200
#python MVA.py -njets 2  -ptv _ptv_250     -fold 1 -weights AbsWeights -mBB_max 500
#python MVA.py -njets 3  -ptv _ptv_150_250 -fold 1 -weights AbsWeights -mBB_max 200
#python MVA.py -njets 3  -ptv _ptv_250     -fold 1 -weights AbsWeights -mBB_max 200

#python MVA.py -njets 2  -ptv _ptv_150_250 -fold 2 -weights AbsWeights -mBB_max 200
python MVA.py -njets 2  -ptv _ptv_250     -fold 2 -weights AbsWeights -mBB_max 500
#python MVA.py -njets 3  -ptv _ptv_150_250 -fold 2 -weights AbsWeights -mBB_max 200
#python MVA.py -njets 3  -ptv _ptv_250     -fold 2 -weights AbsWeights -mBB_max 200

ELAPSED_TIME=$(($SECONDS - $START_TIME))
echo "Finished!" 
echo "$(($ELAPSED_TIME/60)) min $(($ELAPSED_TIME%60)) sec" 

#This is a script for Condor
#To send the job: condor_submit submitCondor.sh
