import os
import time

njets = 2
ptv_region = "_ptv_150_250"

max_depths = [5]
gammas = [0]
min_child_weights = [1]
learning_rates = [0.1]

for max_depth in max_depths:
  for gamma in gammas:
    for min_child_weight in min_child_weights:
      for learning_rate in learning_rates:
        f = open("jobCV.sh", "w")
        f.write("#!/bin/bash \n")
        f.write("source /home/ambroz/setup_env.sh \n")
        f.write("cd /home/ambroz/VHbb/analysis_multiclass \n")
        f.write("python MVA_CV.py -njets " + str(njets) + " -ptv " + ptv_region +  " -weights NoNegWeights " + " -max_depth " + str(max_depth) + " -min_child_weight " + str(min_child_weight) + " -gamma " + str(gamma) + " -learning_rate " + str(learning_rate) + " \n")
        f.close()
        
        os.system("condor_submit submitCondor_CV.sh")
        #time.sleep(10)
        #os.system("rm jobCV.sh")

