from numpy import loadtxt
from xgboost import XGBClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, classification_report
import numpy as np
import pandas as pd
import pickle
import matplotlib.pyplot as plt
import math
import argparse

def main(njets, ptv_region, data_path, weights, fold, mBB_max, dRBB_max, max_depth, gamma, learning_rate, min_child_weight):
  
  ### Load the DataFrames ###
  if fold == 1:
    DataFrame_train      = pd.read_pickle(data_path + '/train'      + str(njets) + 'jet' + ptv_region + '_even.pkl')
    DataFrame_validation = pd.read_pickle(data_path + '/validation' + str(njets) + 'jet' + ptv_region + '_even.pkl')
  elif fold == 2:
    DataFrame_train      = pd.read_pickle(data_path + '/train'      + str(njets) + 'jet' + ptv_region + '_odd.pkl')
    DataFrame_validation = pd.read_pickle(data_path + '/validation' + str(njets) + 'jet' + ptv_region + '_odd.pkl')
  else:
    print("Wrong fold.")
    return
  ###########################

  ### Apply training settings ###
  print("====== Training settings ======")
  print("njets: " + str(njets))
  print("ptv_region: " + ptv_region)
  print("weights: " + weights)
  print("fold: " + str(fold))

  print("Hyper-par")
  print("max_depth: " + str(max_depth))
  print("gamma: " + str(gamma))
  print("learning_rate: " + str(learning_rate))
  print("min_child_weight: " +str(min_child_weight))


  if mBB_max:
    print("Number of training events before requiring mBB < " + str(mBB_max) + ": " + str(DataFrame_train.shape[0]))  
    DataFrame_train = DataFrame_train[DataFrame_train["mBB"] < mBB_max]
    DataFrame_validation = DataFrame_validation[DataFrame_validation["mBB"] < mBB_max]
    print("Number of training events before requiring mBB < " + str(mBB_max) + ": " + str(DataFrame_train.shape[0]))
  
  if dRBB_max:
    print("Number of training events before requiring dRBB < " + str(dRBB_max) + ": " + str(DataFrame_train.shape[0]))
    DataFrame_train = DataFrame_train[DataFrame_train["dRBB"] < dRBB_max]
    DataFrame_validation = DataFrame_validation[DataFrame_validation["dRBB"] < dRBB_max]
    print("Number of training events before requiring dRBB < " + str(dRBB_max) + ": " + str(DataFrame_train.shape[0]))

  if weights == "NoNegWeights":
    print("Number of training events before requiring EventWeight > 0: " + str(DataFrame_train.shape[0]))
    DataFrame_train = DataFrame_train[DataFrame_train["EventWeight"] > 0]
    print("Number of training events after requiring EventWeight > 0: " + str(DataFrame_train.shape[0]))
    print("Number of validation events before requiring EventWeight > 0: " + str(DataFrame_validation.shape[0]))
    DataFrame_validation = DataFrame_validation[DataFrame_validation["EventWeight"] > 0]
    print("Number of validation events before requiring EventWeight > 0: " + str(DataFrame_validation.shape[0]))
  elif weights == "AbsWeights":
    DataFrame_train['EventWeight'] = DataFrame_train['EventWeight'].abs()
    DataFrame_train['NormEventWeight'] = DataFrame_train['NormEventWeight'].abs()
    DataFrame_train['NormEventWeightOneVsAll'] = DataFrame_train['NormEventWeightOneVsAll'].abs()
    DataFrame_validation['EventWeight'] = DataFrame_validation['EventWeight'].abs()
    DataFrame_validation['NormEventWeight'] = DataFrame_validation['NormEventWeight'].abs()
    DataFrame_validation['NormEventWeightOneVsAll'] = DataFrame_validation['NormEventWeightOneVsAll'].abs()
  ##############################
  
  print("Training Dataframe: ")
  print(DataFrame_train.describe())
  print("Validation Dataframe: ")
  print(DataFrame_validation.describe())
  print("===============================")

  ######################
  feature_names = ["mBB", "dRBB", "pTB1", "pTB2", "MET", "dPhiVBB", "nTaus", "bin_MV2c10B1", "bin_MV2c10B2", "softMET"]
  if njets == 3:
    feature_names.append("mBBJ")
    feature_names.append("pTJ3")
    feature_names.append("dRBBJ")
  else:
    feature_names.append("hasFSR")
  print("Training features (" + str(len(feature_names)) + "):")
  print(DataFrame_train[feature_names].head())
  ######################
  
  ### Convert DataFrames in numpy arrays ###
  X_train         = DataFrame_train[feature_names].values
  Y_train         = DataFrame_train[["Class"]].values
  X_validation         = DataFrame_validation[feature_names].values
  Y_validation         = DataFrame_validation[["Class"]].values
  
  weight_scale = 1e7 #To faciliate computation
  #X_train_weights =      DataFrame_train[["NormEventWeight"]].values * weight_scale 
  #X_validation_weights = DataFrame_validation[["NormEventWeight"]].values * weight_scale 
  X_train_weights =      DataFrame_train[["NormEventWeightOneVsAll"]].values * weight_scale 
  X_validation_weights = DataFrame_validation[["NormEventWeightOneVsAll"]].values * weight_scale 
  ##########################################
  
  ### XGB ###
  classifier = "XGB"
  if classifier == "XGB":
  	
    eval_metric = "mlogloss" #merror mlogloss
  
    model = XGBClassifier(objective='multi:softprob', 
  			                  num_class = 5, 
  			                  max_depth = max_depth, 
  			                  n_estimators = 10000,
  			                  learning_rate = learning_rate,
                          feature_names = feature_names,
                          eval_metric = eval_metric, 
                          subsample = 0.9, #Default 1
                          colsample_bytree = 0.9, #Default 1
                          gamma = gamma, #Default 0
                          min_child_weight = min_child_weight, #Default 1
                          tree_method = 'exact',
  			                  nthread = -1)
  
    # Fit the model:
    model.fit(X_train, Y_train.ravel(), 
  	          sample_weight = X_train_weights.ravel(),
              eval_set = [(X_train, Y_train.ravel()), (X_validation, Y_validation.ravel())],
  	          sample_weight_eval_set = [X_train_weights.ravel(), X_validation_weights.ravel()],
              early_stopping_rounds = 30,
              verbose = True) 
  ###########
  
  ### Store the model ###
  f_output = "/data/atlas/atlasdata3/ambroz/VHbb/Models/ThesisHybrid/model_" + classifier + "_" + eval_metric + "_max_depth" + str(max_depth) + "_gamma" + str(gamma).replace('.','')  + "_learning_rate"+ str(learning_rate).replace('.','') + "_min_child_weight" + str(min_child_weight).replace('.','')  + "_jets" + str(njets) + ptv_region + "_fold" + str(fold)
  
  print(f_output)
  pickle.dump(model, open(f_output + ".dat", "wb"))
  model.save_model(f_output + ".model")
  model.save_model(f_output + ".h5")
  
############

if __name__ == '__main__':

  parser = argparse.ArgumentParser(description = 'MVA Training')
  parser.add_argument('-njets', help='Njets: 2 or 3. Default: 2.', default = 2, type = int)
  parser.add_argument('-ptv',   help='Nothing is the inclusive case, or _ptv_150_250, or _ptv_250. Default: _ptv_250.', default = '_ptv_250', type = str)
  parser.add_argument('-data_path', help='Data path.', default = '/data/atlas/atlasdata3/ambroz/VHbb/DataFrames/ThesisHybrid/', type = str)
  parser.add_argument('-weights', help='Options for training: NormalWeights, NoNegWeights (removed Neg weights), AbsWeights (use absolute values). Default: NormalWeights', default = 'NormalWeights', type = str)
  parser.add_argument('-fold',    help='Set fold. Options are 1 and 2. Default: 1.', default = 1, type = int)
  parser.add_argument('-mBB_max', help='Set mBBMax (unit GeV) e.g: 1000', default = False, type = float)
  parser.add_argument('-dRBB_max', help='Set dRBBMax e.g: 2.5', default = False, type = float)
  parser.add_argument('-max_depth', help='Set max_depth e.g: 6', default = 6, type = int)
  parser.add_argument('-gamma', help='Set gamma e.g: 0', default = 0, type = float)
  parser.add_argument('-learning_rate', help='Set learning_rate (eta) e.g: 0.3', default = 0.3, type = float)   
  parser.add_argument('-min_child_weight', help='Set min_child_weight e.g: 1', default = 1, type = float)

  args = parser.parse_args()

  main(args.njets, args.ptv, args.data_path, args.weights, args.fold, args.mBB_max, args.dRBB_max, args.max_depth, args.gamma, args.learning_rate, args.min_child_weight)
