import os
import time
from datetime import datetime

njets = ['2']
ptv_regions = ['_ptv_150_250'] #,'_ptv_150_250', '_ptv_250']
folds = ['1', '2']

max_depths = ['6', '7', '8']
gammas = ['0', '0.2', '0.4']
learning_rates = ['0.01']
min_child_weights = ['1', '3']

#gammas = ['0']
#learning_rates = ['0.05']
#min_child_weights = ['1']

job_index = 190
for njet in njets:
  for ptv_region in ptv_regions:
    for fold in folds:

      for max_depth in max_depths:
        for gamma in gammas:
          for learning_rate in learning_rates:
            for min_child_weight in min_child_weights:

              job_name = 'job_' + str(job_index) 
              f = open(job_name + '.sh', 'w')
        
              #MVA job
              f.write("#!/bin/bash \n")
              f.write("source /home/ambroz/setup_env.sh \n")
              f.write("cd /home/ambroz/VHbb/analysis_multiclass \n")
              f.write("python MVA.py -njets " + njet + " -ptv " + ptv_region + " -fold " + fold  + " -weights AbsWeights -max_depth " + max_depth + " -gamma "+ gamma + " -learning_rate " + learning_rate + " -min_child_weight " + min_child_weight + " \n")
              f.close()
              
              #Condor job with batch settings
              fCondor = open(job_name + '_Condor.sh', "w")
              fCondor.write("executable              = " + job_name + ".sh \n")
              fCondor.write("output                  = output/results.output.$(Cluster) \n")
              fCondor.write("log                     = log/results.log.$(Cluster) \n")
              fCondor.write("stream_output           = True \n")
              fCondor.write("request_cpus            = 4 \n")
              fCondor.write("request_memory          = 16 GB \n")
              fCondor.write("queue \n")
              fCondor.close()
              
              command = "condor_submit " + job_name + "_Condor.sh "
              print(command)              
              os.system(command)
        
              #To keep the load on the cluster sustainable
              now = datetime.now()
              #if now.hour > 18 or now.hour < 6:
              #  time.sleep(0.2*60) #Send a job every X min
              #else:
              #  time.sleep(20*60) #Send a job every Y min
              time.sleep(2)
        
              job_index+=1

