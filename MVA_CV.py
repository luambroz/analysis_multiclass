from numpy import loadtxt
from xgboost import XGBClassifier, cv, DMatrix
from sklearn.model_selection import train_test_split, cross_val_score, GridSearchCV, validation_curve
from sklearn.metrics import accuracy_score, classification_report, log_loss, make_scorer
import numpy as np
import pandas as pd
import pickle
import matplotlib.pyplot as plt
import math
import argparse
import os

def main(njets, ptv_region, data_path, weights, fold, mBB_max, max_depth, min_child_weight, gamma, learning_rate):

  ### Load the DataFrames ###
  if fold == 1:
    DataFrame_train      = pd.read_pickle(data_path + '/train'      + str(njets) + 'jet' + ptv_region + '_even.pkl')
    DataFrame_validation = pd.read_pickle(data_path + '/validation' + str(njets) + 'jet' + ptv_region + '_even.pkl')
  elif fold == 2:
    DataFrame_train      = pd.read_pickle(data_path + '/train'      + str(njets) + 'jet' + ptv_region + '_odd.pkl')
    DataFrame_validation = pd.read_pickle(data_path + '/validation' + str(njets) + 'jet' + ptv_region + '_odd.pkl')
  else:
    print("Wrong fold.")
    return

  ### Apply training settings ###
  print("====== Training settings ======")
  print("njets: " + str(njets))
  print("ptv_region: " + ptv_region)
  print("weights: " + weights)
  print("fold: " + str(fold))
  print("max_depth: " + str(max_depth))
  print("min_child_weight: " + str(min_child_weight))
  print("gamma: " + str(gamma))
  print("learning_rate: " + str(learning_rate))

  if mBB_max:
    print("Number of training events before requiring mBB < " + str(mBB_max) + ": " + str(DataFrame_train.shape[0]))
    DataFrame_train = DataFrame_train[DataFrame_train["mBB"] < mBB_max]
    print("Number of training events before requiring mBB < " + str(mBB_max) + ": " + str(DataFrame_train.shape[0]))

  if weights == "NoNegWeights":
    print("Number of training events before requiring EventWeight > 0: " + str(DataFrame_train.shape[0]))
    DataFrame_train = DataFrame_train[DataFrame_train["EventWeight"] > 0]
    print("Number of training events after requiring EventWeight > 0: " + str(DataFrame_train.shape[0]))
  elif weights == "AbsWeights":
    DataFrame_train['EventWeight'] = DataFrame_train['EventWeight'].abs()
    DataFrame_train['NormEventWeight'] = DataFrame_train['NormEventWeight'].abs()
    DataFrame_train['NormEventWeightOneVsAll'] = DataFrame_train['NormEventWeightOneVsAll'].abs()
  ##############################

  ### Take the column names directly from the DataFrame ###
  feature_names = list(DataFrame_train.columns.values)
  feature_names.remove("Class")
  feature_names.remove("NormEventWeight")
  feature_names.remove("EventWeight")
  feature_names.remove("EventNumber")
  feature_names.remove("NormEventWeightOneVsAll")
  #########################################################

  ### ICHEP settings ###
  do_ICHEP_features = True
  feature_names = ["mBB", "dRBB", "pTB1", "pTB2", "MET", "dPhiVBB", "dEtaBB", "MEff", "nTaus"] #ICHEP + nTaus
  if do_ICHEP_features:
    feature_names = ["mBB", "dRBB", "pTB1", "pTB2", "MET", "dPhiVBB", "dEtaBB", "MEff"]
  if njets == 3:
    feature_names.append("mBBJ")
    feature_names.append("pTJ3")
  ######################

  ### Convert DataFrames in numpy arrays ###
  X_train         = DataFrame_train[feature_names].values
  Y_train         = DataFrame_train[["Class"]].values
  X_validation         = DataFrame_validation[feature_names].values
  Y_validation         = DataFrame_validation[["Class"]].values

  weight_scale = 1e7 #To faciliate computation
  #X_train_weights =      DataFrame_train[["NormEventWeight"]].values * weight_scale 
  #X_validation_weights = DataFrame_validation[["NormEventWeight"]].values * weight_scale 
  X_train_weights =      DataFrame_train[["NormEventWeightOneVsAll"]].values * weight_scale
  X_validation_weights = DataFrame_validation[["NormEventWeightOneVsAll"]].values * weight_scale
  ##########################################
  
  #For CV, put back together the training and validation sets.
  X_train = np.concatenate((X_train, X_validation), axis=0)
  Y_train = np.concatenate((Y_train, Y_validation), axis=0)
  X_train_weights = np.concatenate((X_train_weights, X_validation_weights), axis=0)
  
  dtrain = DMatrix(data = X_train, 
                   label = Y_train, 
                   weight = X_train_weights
                  )
  
  n_estimators = 10000
  early_stopping_rounds = 50
  
  params = {
      'num_class' : 6,
      'objective' : 'multi:softprob',
      'eval_metric' : 'mlogloss',
      'n_estimators' : n_estimators,
      'early_stopping_rounds' : early_stopping_rounds,
      'n_jobs' : - 1,
      #Structure of the trees
      'max_depth' : max_depth,         
      'min_child_weight' : min_child_weight,        
      'gamma' : gamma,                   
      #Learning speed
      'learning_rate' : learning_rate,    
  }
  
  res = cv(params, dtrain, nfold = 10, stratified = True, num_boost_round = n_estimators, early_stopping_rounds = early_stopping_rounds)
  
  res.to_csv("CV_results/results_" + str(njets) + "njets" + ptv_region + "_max_depth" + str(max_depth) + "_min_child_weight" + str(min_child_weight) + "_gamma" + str(gamma) + "_learning_rate" + str(learning_rate) + ".csv")

  best_error = res['test-mlogloss-mean'].min()


  file_best_errors = "CV_results/best_errors_" + str(njets) + "njets" + ptv_region + ".txt"
  if os.path.isfile(file_best_errors):
    f = open(file_best_errors, "a")
  else:
    f = open(file_best_errors, "w")
  f.write(str(best_error)  + "<-- this is min test-mlogloss-mean for njets: " + str(njets) + ", ptv_region: " + ptv_region + ", max_depth: " + str(max_depth) + ", min_child_weight: " + str(min_child_weight) + ", gamma: " + str(gamma) + ", learning_rate: " + str(learning_rate) + '\n')
  f.close()
 

  


if __name__ == '__main__':

  parser = argparse.ArgumentParser(description = 'MVA CV')

  parser.add_argument('-njets', help='Njets: 2 or 3. Default: 2.', default = 2, type = int)
  parser.add_argument('-ptv',   help='Nothing is the inclusive case, or _ptv_150_250, or _ptv_250. Default: _ptv_250.', default = '_ptv_250', type = str)
  parser.add_argument('-data_path', help='Data path.', default = '/data/atlas/atlasdata3/ambroz/VHbb/DataFrames/NormalWeights/', type = str)
  parser.add_argument('-weights', help='Options for training: NormalWeights, NoNegWeights (removed Neg weights), AbsWeights (use absolute values). Default: NormalWeights', default = 'NormalWeights', type = str)
  parser.add_argument('-fold',    help='Set fold. Options are 1 and 2. Default: 1.', default = 1, type = int)
  parser.add_argument('-mBB_max', help='Set mBBMax (unit GeV) e.g: 1000', default = False, type = float)

  parser.add_argument('-max_depth', help='max_depth. Default: 3.', default = 3, type = int)
  parser.add_argument('-min_child_weight', help='min_child_weight. Default: 1.', default = 1, type = float)
  parser.add_argument('-gamma', help='gamma. Default: 0.', default = 0, type = float)
  parser.add_argument('-learning_rate', help='learning_rate. Default: 0.1', default = 0.1, type = float)

  args = parser.parse_args()

  main(args.njets, args.ptv, args.data_path, args.weights, args.fold, args.mBB_max, args.max_depth, args.min_child_weight, args.gamma, args.learning_rate)
