executable              = jobCV.sh
output                  = output/results.output.$(Cluster)
error                   = error/results.error.$(Cluster)
log                     = log/results.log.$(Cluster)
stream_output           = True
request_cpus            = 16
request_memory          = 4 GB
queue
